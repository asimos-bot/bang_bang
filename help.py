import random
from player import *
from ai import *
#em uma linha é mais legal =D
def random_position(playerpos):
    return (lambda x: x if x!=playerpos else random_position(playerpos))(random.randint(0,4))

def initialize_players(game_map, IA, window):
	window_width = window.width
	window_height = window.height
	
	#players settings
	player1 = Player("imgs/Player1.png", window)
	player1.reload_key = "b"
	player1.shoot_key="v"
	player1.up_key="q"
	player1.down_key="a"
	player1.orientation = 1
	who_shot_last = 0
	if not IA:
		player2 = Player("imgs/cangaceiro.png", window)
		player2.reload_key = "p"
		player2.shoot_key="o"
		player2.up_key="up"
		player2.down_key="down"
		player2.orientation=-1
	else:
		player2 = Bot([12,10,5], "ann_choice.npz", "imgs/cangaceiro.png", window)
		player2.player.orientation=-1
		player2.orientation=-1

	player1.set_position(-16, window_height/2 - player1.height/2)
	player2.set_position(window_width - player2.width -10, window_height/2 - player2.height/2)

	player1.pos = random_position(-1)
	player2.pos = random_position(player2.pos)
	if IA: player2.update()
	#put players in the battlefield LET THE BLOODBATH START HUAHUAHUAHUA
	player1.move(game_map.map)
	player2.move(game_map.map)
	
	return player1, player2

