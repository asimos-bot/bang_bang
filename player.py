from PPlay.sprite import *

class Player(object):

	def __init__(self, img_file, window):
	
		#basic game entity attributes
		self.window = window
		self.sprite = Sprite(img_file)



		self.speed_x=0
		self.speed_y=0
		
		self.height = self.sprite.height
		self.width = self.sprite.width

		self.x = 0
		self.y = 0

		self.orientation = None

		#player status
		self.action_key_lock = False
		self.move_key_lock = False
		self.alive=True
		#actions
		self.shoot_key=None
		self.reload_key=None

		self.bullets=1
		self.current_action=None
		
		#movement
		self.pos=2
		self.up_key=None
		self.down_key=None

		self.current_movement=None
		self.bullet_sprite = Sprite("imgs/bullet.png")

		# bullet
		self.bullet = 0
		self.bulletSpeed = 1500

	def get_action(self):
	
		keyboard = self.window.get_keyboard()
		
		keys = { 
		 'shoot': keyboard.key_pressed(self.shoot_key) and not self.action_key_lock,
		 'reload': keyboard.key_pressed(self.reload_key) and not self.action_key_lock,
		}
		#check if just one key was pressed
		if(sum(keys.values())==1):
		
			self.action_key_lock=True

			if(keys['shoot']):
				self.current_action = 'shoot'


			elif(keys['reload']):
				self.current_action = 'reload'

		elif(not self.action_key_lock):
			self.current_action = None


	def verifica(self):
		return self.x

	def get_movement(self):
		
		keyboard = self.window.get_keyboard()
		
		keys = { 
		 'up': keyboard.key_pressed(self.up_key) and not self.move_key_lock,
		 'down': keyboard.key_pressed(self.down_key) and not self.move_key_lock,
		}
		#check if just one key was pressed
		if(sum(keys.values())==1):

			self.move_key_lock=True

			if(keys['up']):
				self.current_movement = 'up'
			elif(keys['down']):
				self.current_movement = 'down'
		elif(not self.move_key_lock):
			self.current_movement = None
			
	def move(self, game_map):
	
		if(self.current_movement=='down'):
			
			#if player is already in the lowest spot nothing will happen
			if(self.pos>0):
				self.pos-=1
		
		elif(self.current_movement=='up'):
		
			#if player is already in the highest spot nothing will happen
			if(self.pos<len(game_map)-1):
				self.pos+=1
				
		self.set_position(self.x, game_map[self.pos])
		self.y = game_map[self.pos]


	def player_shoot(self):
		# instancia da bala
		self.bullet = Sprite("imgs/bullet_action.png")

		# calculando a posição da bala em relação so atirador
		if self.orientation == 1:
			bullet_x = self.x + self.width

		elif self.orientation == -1:
			bullet_x = self.x

		# esse /4 é para a bala ser mais próxima da parte de cima do jogador
		bullet_y = self.y + self.height / 4

		self.bullet.set_position(bullet_x, bullet_y)

	def bullet_move(self):
		# variável para calcular movimento em x
		movimento = self.bulletSpeed * self.window.delta_time() * self.orientation
		self.bullet.move_x(movimento)

	def set_position(self, x, y):

		self.x = x
		self.y = y

		self.sprite.set_position(x,y)

	def enable_action(self):
		self.action_key_lock=False

	def enable_move(self):
		self.move_key_lock=False

	def bullet_count(self):
		if self.x < 400:
			for i in range(self.bullets):
				self.bullet_sprite.set_position(i*20 + 10 , 0)
				self.bullet_sprite.draw()
		else:
			for i in range(self.bullets):
				self.bullet_sprite.set_position(- i * 20 + self.x + self.width - self.bullet_sprite.width, 0)
				self.bullet_sprite.draw()

	def button_set(self):
		if self.x < 400:
			if (self.current_action != None):
				action_button = Sprite("imgs/action_button_pressed.png")
				action_button.set_position(16, 337)
				action_button.draw()
			else:
				action_button = Sprite("imgs/action_button.png")
				action_button.set_position(16, 335)
				action_button.draw()

			if (self.move_key_lock == True):
				move_button = Sprite("imgs/move_button_pressed.png")
				move_button.set_position(100, 337)
				move_button.draw()
			else:
				move_button = Sprite("imgs/move_button.png")
				move_button.set_position(100, 335)
				move_button.draw()
		else:
			if (self.current_action != None):
				action_button = Sprite("imgs/action_button_pressed.png")
				action_button.set_position(720, 337)
				action_button.draw()
			else:
				action_button = Sprite("imgs/action_button.png")
				action_button.set_position(720, 335)
				action_button.draw()

			if (self.move_key_lock == True):
				move_button = Sprite("imgs/move_button_pressed.png")
				move_button.set_position(636, 337)
				move_button.draw()
			else:
				move_button = Sprite("imgs/move_button.png")
				move_button.set_position(636, 335)
				move_button.draw()


	def update(self):
		
		# delta_x = self.window.delta_time() * self.speed_x
		# delta_y = self.window.delta_time() * self.speed_y
		# self.sprite.move_x(delta_x)
		# self.sprite.move_y(delta_y)
		

		#in case a new sprite was loaded and don't have the coordinates in it
		self.set_position( self.x, self.y )
		
		#self.x += delta_x
		#self.y += delta_y

		self.button_set()
		self.bullet_count()
		self.sprite.draw()

		if self.bullet != 0:
			self.bullet_move()
			self.bullet.draw()


