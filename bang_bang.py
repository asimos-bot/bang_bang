#!/usr/bin/env python3
from PPlay.window import *
from player import *
from game_map import *
from round import *
from action import *
from menu import *
from PPlay.gameimage import *
from help import *
from ai import *
from PPlay.sprite import *
from PPlay.sound import *
import time

#window settings
window_width = 800
window_height = 800

window = Window(window_width, window_height)
window.set_title("BANG BANG!")

fundo = GameImage("imgs/fundo.png")

#keyboard and mouse settings
keyboard = Window.get_keyboard()
mouse = Window.get_mouse()

#buttons
action_button = Sprite("imgs/action_button.png")
move_button = Sprite("imgs/move_button.png")

#sound settings
sound = Sound("sounds/soundtrack.ogg")
sound.play()
sound.set_volume(10)
shoot_sound = Sound("sounds/shoot.ogg")
shoot_sound.set_volume(100)

#move background
front = False

#round
game_round = Round(8, window)

#game map
game_map = GameMap(5, window)

#clock for round
#roundClock = Sprite("imgs/1.png")


#flash after standoff
current_flash = False
flash = Sprite("imgs/flash_sprite.png", frames=5)
flash.set_total_duration(3000)
flash.set_loop(False)


#IA IS ON
IA=True
IA_hardening=-2 #can make game slower if too high

game_state = 0

#players not initialized



game_state = 0

player1, player2 = initialize_players(game_map, IA, window)
Aux = []

if game_state == 0:
	game_state = menu(window)
if game_state == 3:
	IA = False
	player1, player2 = initialize_players(game_map, IA, window)


	game_state = 1
if game_state == 1:
	Aux = []
	Aux.append("imgs/cangaceiro.png")
	Aux.append("imgs/cangaceiro_shoot.png")
	Aux.append("imgs/cangaceiro_dead.png")
	fundo = GameImage("imgs/fundo.png")
	player2.sprite = Sprite(Aux[0])
	player2.move(game_map.map)



elif game_state == 2:
	Aux = []
	Aux.append("imgs/Scape.png")
	Aux.append("imgs/Scape_shoot.png")
	Aux.append("imgs/Scape_dead.png")
	fundo = GameImage("imgs/fundo2.png")
	player2.sprite = Sprite(Aux[0])
	player2.move(game_map.map)


#game loop
while(1):

	#update round clock
	game_round.clock_update()
	game_round.countdown()



	fundo.draw()
	#roundClock.draw()

	#handle players movement
	player1.get_movement()
	if not IA: player2.get_movement()

	#action_button.draw()
	#move_button.draw()
	#handle players actions
	player1.get_action()
	if player1.current_action != "shoot":
		who_shot_last = player1.verifica()
	if not IA:
		player2.get_action()
		if player2.current_action != "shoot":
			who_shot_last = player2.verifica()

	#update round clock
	game_round.clock_update()
	if player1.alive and player2.alive: game_round.countdown()

	#enable movement and action
	if( game_round.round_over() ):

		#make IA do IA stuff
		if IA: player2.do_IA_stuff(player1.pos, player1.bullets)
		if IA and IA_hardening>0: player2.ann.train_mini_batch(train_x, train_y, 0.05, IA_hardening, 0, 5, 0.05)

		player1.move(game_map.map)
		player2.move(game_map.map)
		if player2.alive == False:
			fundo = GameImage("imgs/p1_wins.png")
			front = True
		if player1.alive == False:
			fundo = GameImage("imgs/game_over.png")
			front = True



		move_button = Sprite("imgs/move_button.png")
		move_button.set_position(100, 335)

		action_button = Sprite("imgs/action_button.png")
		action_button.set_position(16, 335)


		player1.sprite = Sprite("imgs/Player1.png")
		player2.sprite = Sprite(Aux[0])
		#player1.set_position(-16, window_height / 2 - player1.height / 2)
		#player2.set_position(window_width - player2.width - 10, window_height / 2 - player2.height / 2)


		if (player1.current_action == 'shoot'):
			player1.sprite = Sprite("imgs/Player1_shoot.png")
			if player2.bullets > 0:
				player1.player_shoot()
				shoot_sound.play()

		if (player2.current_action == 'shoot'):
			player2.sprite = Sprite(Aux[1])
			if player2.bullets > 0:
				player2.player_shoot()
				shoot_sound.play()



		if (player1.current_action == 'reload') and player1.bullets < 3:
			player1.bullets += 1

		if (player2.current_action == 'reload') and player2.bullets < 3:
			player2.bullets += 1

		#habilita captura de input para nova rodada
		player1.enable_action()
		if not IA: player2.enable_action()

		player1.enable_move()
		if not IA: player2.enable_move()

		if( player1.current_action == 'shoot' or player2.current_action == 'shoot' ):

			stand_off = shooting(player1, player2)
			if stand_off:
				flash.set_curr_frame(0)
				current_flash = True


				if who_shot_last < 400:
					player1.alive = False

				else:
					player2.alive = False




		if player2.alive == False:

				player2.sprite = Sprite(Aux[2])
				player2.set_position(window_width - player2.width -10, player1.y)





		if player1.alive == False:
			player1.sprite = Sprite("imgs/Player1_Dead.png")
			player1.set_position(-16, player2.y)


	# update players
	player1.update()
	player2.update()
	if current_flash:
		flash.draw()
		if not flash.is_playing():
			current_flash = False
		flash.update()
	if front == True:
		fundo.draw()

	window.update()
