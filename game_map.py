class GameMap(object):

	def __init__(self, num_spots, window, bottom_offset = 150, top_offset=300):
	
		self.num_spots = num_spots
		self.window = window
		
		self.map = []
		
		gap_between_player_spots = (self.window.height-top_offset-bottom_offset)/self.num_spots

		for i in range(self.num_spots):
			self.map.append(window.height-i*gap_between_player_spots-bottom_offset)
