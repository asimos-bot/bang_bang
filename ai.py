#!/usr/bin/env python3
from ann.ann_choice import *
from PPlay.sprite import *
from player import *

'''
BIG IA ARRAY TRAINING STUFF BLAH BLAH BLAH PASSING BY, JUST IGNORE IT
'''
train_x=np.array([[1,0,0,0,0,1, 1,0,0,0,0,0],#1
		[0,1,0,0,0,1, 1,0,0,0,0,0],#2
		[0,0,1,0,0,1, 1,0,0,0,0,0],#3
		[0,0,1,0,0,0, 0,0,1,0,0,1],#4
		[0,1,0,0,0,0, 0,0,1,0,0,1],#5
		[1,0,0,0,0,1, 0,0,0,1,0,1],#6
		[0,0,0,1,0,1, 0,0,0,1,0,1],#7
		[0,1,0,0,0,1, 0,0,1,0,0,1],#8
		[0,0,1,0,0,0, 0,0,1,0,0,1],#9
		[1,0,0,0,0,1, 0,0,0,1,0,0],#10
		[0,0,1,0,0,1, 1,0,0,0,0,0],#11
		[0,0,1,0,0,1, 1,0,0,0,0,1],#12
		[0,1,0,0,0,1, 0,0,0,0,1,1],#13
		[0,1,0,0,0,0, 0,0,0,0,1,1],#14
		[1,0,0,0,0,0, 1,0,0,0,0,1],#15
		[0,0,1,0,0,1, 1,0,0,0,0,1],#16
		[0,0,1,0,0,0, 1,0,0,0,0,1],#17
		[0,0,1,0,0,0, 0,1,0,0,0,0],#18
		[0,1,0,0,0,1, 1,0,0,0,0,1],#19
		[0,1,0,0,0,1, 1,0,0,0,0,0],#20
		[1,0,0,0,0,0, 0,0,0,0,1,1],#21
		[1,0,0,0,0,1, 0,0,0,0,1,1],#22
		[0,0,0,0,1,1, 1,0,0,0,0,1],#23
		[0,0,0,0,1,0, 1,0,0,0,0,1],#24
		[0,1,0,0,0,1, 0,0,1,0,0,1],#25
		[0,0,0,1,0,0, 0,0,0,0,1,1],#26
		[1,0,0,0,0,1, 0,0,1,0,0,1],#27
		[1,0,0,0,0,1, 0,0,0,0,1,1],#28
		[0,1,0,0,0,1, 0,1,0,0,0,1],#29
		[0,0,1,0,0,1, 0,0,1,0,0,1],#30
		[0,0,0,1,0,1, 0,0,0,1,0,1],#31
		[1,0,0,0,0,1, 0,0,0,1,0,1],#32
		[1,0,0,0,0,1, 0,0,0,1,0,0],#33
		[1,0,0,0,0,0, 0,0,0,1,0,0],#34
		[1,0,0,0,0,0, 0,0,0,1,0,1],#35
		[0,0,0,1,0,1, 1,0,0,0,0,1],#36
		[0,0,0,1,0,1, 1,0,0,0,0,0],#37
		[0,0,0,1,0,0, 1,0,0,0,0,1],#38
		[0,0,0,1,0,0, 1,0,0,0,0,0],#39
		],
		dtype=float)
train_y=np.array([[0,1,0,1,0],#1
		[0,1,0,1,1],#2
		[0,1,1,0,1],#3
		[1,0,0,0,1],#4
		[1,0,1,0,0],#5
		[0,1,1,0,0],#6
		[1,0,1,1,1],#7
		[1,0,0,0,1],#8
		[1,0,1,1,0],#9
		[0,1,1,0,0],#10
		[0,1,0,0,1],#11
		[1,0,0,1,1],#12
		[1,0,0,1,0],#13
		[1,0,0,1,0],#14
		[1,0,0,0,1],#15
		[0,1,1,0,0],#16
		[0,1,1,0,0],#17
		[0,1,1,0,0],#18
		[0,1,1,0,0],#19
		[0,1,0,1,0],#20
		[0,1,0,1,0],#21
		[0,1,0,1,0],#22
		[0,1,1,0,0],#23
		[1,0,1,0,1],#24
		[0,1,0,1,1],#25
		[0,1,0,1,1],#26
		[0,1,0,1,0],#27
		[0,1,1,0,0],#28
		[1,0,1,1,1],#29
		[1,0,1,1,1],#30
		[1,0,1,1,1],#31
		[0,1,0,1,0],#32
		[0,1,0,1,0],#33
		[0,1,0,1,0],#34
		[0,1,0,1,0],#35
		[0,1,1,0,0],#36
		[0,1,1,0,0],#37
		[0,1,1,0,0],#38
		[0,1,1,0,0],#39
		],
		dtype=float)
'''
	CONGRATULATIONS! YOU JUST IGNORED IT SUCESSFULLY (or not, in that case ask forgiviness for our future machine overlords)
'''

class Bot(object):

	def __init__(self, layers, filename, sprite_path, window):
		self.ann = Neural_Network(layers, filename)
		self.ann.load(filename)
		self.player = Player(sprite_path, window)
		self.sprite = self.player.sprite
		self.current_action=None
		self.pos=None
		self.bullets=1
		self.alive=True #☉_☉ wut
		self.width = self.sprite.width
		self.height = self.sprite.height
		self.x=0
		self.y=0
		
	#player class stuff (for compatibility with multiplayer code)
	def set_position(self, x, y):
		self.player.set_position(x,y)
		self.pos = self.player.pos
		self.x = self.player.x
		self.y = self.player.y
		
	def move(self, game_map):
		self.player.move(game_map)
		self.pos = self.player.pos
		self.x = self.player.x
		self.y = self.player.y

	def player_shoot(self):
		self.player.player_shoot()
		
	def update(self):
		self.player.update()
		self.player.pos = self.pos
		self.player.bullets = self.bullets
		self.player.sprite = self.sprite
		self.x = self.player.x
		self.y = self.player.y

	def do_IA_stuff(self, enemy_pos, enemy_bullets):
		enemy_pos_arr = [0,0,0,0,0]
		enemy_pos_arr[enemy_pos]=1
		my_pos_arr = [0,0,0,0,0]
		my_pos_arr[self.pos]=1
		
		forward=[]
		forward.extend(enemy_pos_arr)
		forward.append(enemy_bullets>0)
		forward.extend(my_pos_arr)
		forward.append(self.bullets>0)

		results = self.ann.forward(forward)
		#move
		if(results[2]>results[3] and results[2]>results[4] and self.pos<4):
			self.player.pos+=1
		elif(results[3]>results[2] and results[3]>results[4] and self.pos>0):
			self.player.pos-=1
		#act
		if(results[0]>results[1] and self.bullets>0):
			self.current_action='shoot'
			self.player.current_action='shoot'
		else:
			self.current_action='reload'
			self.player.current_action='reload'
		print("results:",results)

if( __name__ == "__main__" ):
	#IT'S MACHINE LEARNING TIME BABY
	#mr robot trademark copyright and other license jibrish for you

	#uncomment the next lines to have always the same random numbers
	#seed=1 
	#np.random.seed(seed)
	#random.seed(seed)

	'''
	INPUT                   OUTPUT
	enemy pos4
	enemy pos3
	enemy pos2
	enemy pos1
	enemy pos0              shoot
	enemy bullets           reload
	my pos4                 move++
	my pos3
	my pos2
	my pos1
	my pos0
	my bullets              move--
                                don't move
	'''
	#Neural_Network = layers,filename=None -> arguments for our ANN
	nn = Neural_Network([12,10,5], "ann_choice.npz")
	
	nn.load("ann_choice.npz")

	#arguments for mini-batch learning
	#train_mini_batch: (x,y,learning_rate,epochs,Lambda=0,mini_batch_size=32,momentum=0)
	nn.train_mini_batch(train_x, train_y, 0.05, 10000, 0.01, 5, 0)
