import os
import gzip
import pickle
import random

import numpy as np

def load_mnist(path,filename):

    data_file = gzip.open(os.path.join(os.curdir, path, filename), 'rb')
    u = pickle._Unpickler(data_file)
    u.encoding = 'latin1'
    p = u.load()

    training_data, validation_data, test_data = p
    data_file.close()

    training_inputs = [np.reshape(x, (784, 1)) for x in training_data[0]]
    training_results = [vectorized_result(y) for y in training_data[1]]
    training_data = zip(training_inputs, training_results)

    validation_inputs = [np.reshape(x, (784, 1)) for x in validation_data[0]]
    validation_results = validation_data[1]
    validation_data = zip(validation_inputs, validation_results)

    test_inputs = [np.reshape(x, (784, 1)) for x in test_data[0]]
    test_data = zip(test_inputs, test_data[1])
    return training_data, validation_data, test_data

def vectorized_result(y):
    e = np.zeros((10, 1))
    e[y] = 1.0
    return e

def unzip(array):

    a = []
    b = []

    for x,y in array:

        a.append(x)
        b.append(y)

    a = np.array((a),dtype=float)
    b = np.array((b),dtype=float)

    return a,b

def shuffle_arrays(a,b):

    sa = []
    sb = []
    i = [x for x in range(len(a))]
    random.shuffle(i)

    sa = [a[i[x]] for x in range(len(a))]
    sb = [b[i[x]] for x in range(len(a))]

    return sa,sb
