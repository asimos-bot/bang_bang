#!/usr/bin/env python3
from .activations import *
from .collector import *
import time,random,os
import numpy as np

class Neural_Network(object):

    def __init__(self,layers,filename=None):

        self.filename = filename

        self.n = len(layers) - 1

        self.w = []

        self.b = []

        for i in range(self.n):

            low = -pow(layers[i],-0.5)

            high = -low

            rows = layers[i]

            columns = layers[i+1]

            self.w.append(np.random.uniform(low,high,(rows,columns)))
            self.b.append(random.random())

    def forward(self,x):
        #Reset/Initialize all activation and activity
        self.z = [None] * self.n
        self.a = [None] * self.n

        #second layer activity and activation
        self.z[0] = np.dot(x,self.w[0]) + self.b[0]
        self.a[0] = sigmoid(self.z[0])

        #hidden layers(except first) activity and activation
        for i in range(1,self.n-1):

            self.z[i] = np.dot(self.a[i-1],self.w[i]) + self.b[i]
            self.a[i] = Relu(self.z[i])

        #output layer activation and activity
        self.z[self.n - 1] = np.dot(self.a[self.n-2],self.w[self.n-1]) + self.b[self.n - 1]
        self.a[self.n - 1] = sigmoid(self.z[self.n-1])

        return self.a[self.n-1]

    def costFunctionPrime(self,x,y,Lambda):

        yHat = self.forward(x)

        delta = [None] * self.n
        dJd = [None] * self.n

        #output layer
        delta[self.n-1] = np.multiply((yHat - y),sigmoid_prime(self.z[self.n-1]))
        dJd[self.n-1] = np.dot(self.a[self.n-2].T,delta[self.n-1]) + Lambda * self.w[self.n-1]

        for i in range(2,self.n):

            #hidden layers(except first hidden layer)
            delta[self.n - i] = np.dot(delta[self.n-i+1],self.w[self.n-i+1].T) * Relu_prime(self.z[self.n-i])
            dJd[self.n - i] = np.dot(self.a[self.n - i -1].T,delta[self.n - i]) + Lambda * self.w[self.n - i]

        #second layer
        delta[0] = np.dot(delta[1],self.w[1].T) * sigmoid_prime(self.z[0])
        dJd[0] = np.dot(x.T,delta[0]) + Lambda * self.w[0]

        for i in range(self.n):

            dJd.append(np.mean(delta[i]))

        return dJd

    def train(self,x,y,learning_rate,epochs,Lambda=0,momentum=0,mini=False):

        if(mini == False):

            start = time.clock()

        iteration = 0

        derivatives= self.costFunctionPrime(x,y,Lambda)

        while(iteration<=epochs):

            for i in range(self.n):

                self.w[i] -= derivatives[i]*learning_rate + self.w[i]*momentum
                self.b[i] -= derivatives[i+self.n]*learning_rate + self.b[i]*momentum

            derivatives= self.costFunctionPrime(x,y,Lambda)
            iteration += 1

            if(mini==False):

                print("{}/{}".format(iteration,epochs))

        if(mini==False):

            print("complete\n")

            end = time.clock()

            time_training = end - start

            print("Time:" + str(time_training))

            if(self.filename != None):

                np.savez(self.filename,self.w,self.b)

    def train_mini_batch(self,x,y,learning_rate,epochs,Lambda=0,mini_batch_size=32,momentum=0):

        start = time.clock()

        iteration = 0

        rest = len(x) % mini_batch_size

        dx = np.delete(x,[i for i in range(rest)],0)
        dy = np.delete(y,[i for i in range(rest)],0)

        n_of_mini_batches = int(len(dx)/mini_batch_size)

        while(iteration < epochs):

            i = random.randrange(0,n_of_mini_batches)

            rx = []
            ry = []

            for w in range(mini_batch_size):

                index = i*mini_batch_size+w

                rx.append(dx[index])
                ry.append(dy[index])

            rx,ry = shuffle_arrays(rx,ry)

            rx = np.array((rx),dtype=float)
            ry = np.array((ry),dtype=float)

            self.train(rx,ry,learning_rate,1,Lambda,momentum,True)

            print("{}/{}".format(iteration,epochs))
            iteration += 1

        print("complete")

        end = time.clock()

        time_training = end - start

        print("Time:" + str(time_training))

        if(self.filename != None):

            np.savez(self.filename,self.w,self.b)

    def load(self,filename):

        arrays = np.load(filename, allow_pickle=True)
        self.w = arrays['arr_0']
        self.b = arrays['arr_1']
