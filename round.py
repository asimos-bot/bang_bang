from PPlay.window import *
from player import *
from game_map import *
from round import *
from action import *
from PPlay.gameimage import *

class Round(object):
	def __init__(self, round_time, window):
	
		self.seconds_sum=0
		self.round_time = round_time
		self.window = window
	
	def clock_update(self):

		self.seconds_sum+=self.window.delta_time()
	def round_over(self):
		
		result = (self.seconds_sum >= self.round_time)
		if( result ):
			self.seconds_sum = 0
		return result

	def countdown(self):
		if self.seconds_sum < 2:

			countdown_time = Sprite("imgs/shoot.png")
			countdown_time.set_position(400 - countdown_time.width/2 , 400 - countdown_time.height/2)
			countdown_time.draw()
		if self.seconds_sum < 4 and self.seconds_sum >= 2:

			countdown_time = Sprite("imgs/3.png")
			countdown_time.set_position(400 - countdown_time.width/2 , 400 - countdown_time.height/2)
			countdown_time.draw()
		if self.seconds_sum < 6 and self.seconds_sum >= 4:

			countdown_time = Sprite("imgs/2.png")
			countdown_time.set_position(400 - countdown_time.width/2 , 400 - countdown_time.height/2)
			countdown_time.draw()
		if self.seconds_sum < 8 and self.seconds_sum >= 6:

			countdown_time = Sprite("imgs/1.png")
			countdown_time.set_position(400 - countdown_time.width/2 , 400 - countdown_time.height/2)
			countdown_time.draw()
