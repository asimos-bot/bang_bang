from PPlay.sprite import *
from PPlay.gameimage import *


def menu(janela):

    # window creation.
    window = janela
    window.set_title("Menu")
    fundo = GameImage("imgs/tela_inicio.png")

    # setting background and mouse inputs.
    mouse = window.get_mouse()

    # creating sprites.
    sprite_singlep = Sprite("imgs/singlep_sprite.png")
    sprite_singlep_hovered = Sprite("imgs/singlep_sprite_hovered.png")

    sprite_multip = Sprite("imgs/multip_sprite.png")
    sprite_multip_hovered = Sprite("imgs/multip_sprite_hovered.png")

    #sprite_sair = Sprite("sprites/menu/retang.png")
    #sprite_sair_2 = Sprite("sprites/menu/retang_2.png")

    # setting sprites positions.
    sprite_width = sprite_singlep.width
    sprite_height = sprite_singlep.height

    pos_sprites_x = (window.width/2) - (sprite_width/2)
    pos_sprites_y = window.height/10

    sprite_singlep.set_position(pos_sprites_x, ((pos_sprites_y)*6.5) - (sprite_height/2))
    sprite_singlep_hovered.set_position(pos_sprites_x, ((pos_sprites_y)*6.5) - (sprite_height/2))

    sprite_multip.set_position(pos_sprites_x, ((pos_sprites_y)*8.5) - (sprite_height/2))
    sprite_multip_hovered.set_position(pos_sprites_x, ((pos_sprites_y)*8.5) - (sprite_height/2))

    # sprite_sair.set_position(pos_sprites_x, ((pos_sprites_y)*9) - (sprite_height/2))
    # sprite_sair_2.set_position(pos_sprites_x, ((pos_sprites_y)*9) - (sprite_height/2))

    mouse_click_tick = 0
    mouse_click_delay = 0.5

    def update_counters(click_tick):
        click_tick += window.delta_time()
        return click_tick

    while True:

            window.set_background_color((0, 0, 0))
            mouse_click_tick = update_counters(mouse_click_tick)
            fundo.draw()
            pos_mouse = mouse.get_position()

            if pos_sprites_x < pos_mouse[0] < (pos_sprites_x + sprite_width):
                if sprite_singlep.y < pos_mouse[1] < (sprite_singlep.y + sprite_singlep.height):
                    sprite_singlep_hovered.draw()

                    if mouse.is_button_pressed(1) and mouse_click_tick > mouse_click_delay:
                        game_state = menu_singlep(window)
                        return game_state

                else:
                    sprite_singlep.draw()

                if sprite_multip.y < pos_mouse[1] < (sprite_multip.y + sprite_multip.height):
                    sprite_multip_hovered.draw()
                    if mouse.is_button_pressed(1) and mouse_click_tick > mouse_click_delay:
                        game_state = 3
                        return game_state

                else:
                    sprite_multip.draw()


                # if sprite_sair.y < pos_mouse[1] < (sprite_sair.y + sprite_sair.height):
                #     sprite_sair_2.draw()
                #     if mouse.is_button_pressed(1) and mouse_click_tick > mouse_click_delay:
                #         exit()
                # else:
                #     sprite_sair.draw()

                mouse_click_tick = update_counters(mouse_click_tick)

            else:
                sprite_singlep.draw()
                sprite_multip.draw()
                #sprite_sair.draw()
                update_counters(mouse_click_tick)

            window.update()


def menu_singlep(janela):

    # window creation.
    window = janela
    window.set_title("Menu")
    fundo = GameImage("imgs/tela_fases.png")

    # setting background and mouse inputs.
    mouse = window.get_mouse()

    # creating sprites.
    sprite_fase1 = Sprite("imgs/fase1_sprite.png")
    sprite_fase1_hovered = Sprite("imgs/fase1_sprite_hovered.png")

    sprite_fase2 = Sprite("imgs/fase2_sprite.png")
    sprite_fase2_hovered = Sprite("imgs/fase2_sprite_hovered.png")

    #sprite_sair = Sprite("sprites/menu/retang.png")
    #sprite_sair_2 = Sprite("sprites/menu/retang_2.png")

    # setting sprites positions.
    sprite_width = sprite_fase1.width
    sprite_height = sprite_fase1.height

    pos_sprites_x = window.width/10
    pos_sprites_y = window.height/10

    sprite_fase1.set_position(pos_sprites_x * 2, ((pos_sprites_y)*5) - (sprite_height/2))
    sprite_fase1_hovered.set_position(pos_sprites_x * 2, ((pos_sprites_y)*5) - (sprite_height/2))

    sprite_fase2.set_position(pos_sprites_x * 6, ((pos_sprites_y)*5) - (sprite_height/2))
    sprite_fase2_hovered.set_position(pos_sprites_x * 6, ((pos_sprites_y)*5) - (sprite_height/2))

    # sprite_sair.set_position(pos_sprites_x, ((pos_sprites_y)*9) - (sprite_height/2))
    # sprite_sair_2.set_position(pos_sprites_x, ((pos_sprites_y)*9) - (sprite_height/2))

    mouse_click_tick = 0
    mouse_click_delay = 0.5

    def update_counters(click_tick):
        click_tick += window.delta_time()
        return click_tick

    while True:

            window.set_background_color((0, 0, 0))
            mouse_click_tick = update_counters(mouse_click_tick)
            fundo.draw()
            pos_mouse = mouse.get_position()

            if sprite_fase1.y < pos_mouse[1] < (sprite_fase1.y + sprite_height):
                if sprite_fase1.x < pos_mouse[0] < (sprite_fase1.x + sprite_fase1.width):
                    sprite_fase1_hovered.draw()

                    if mouse.is_button_pressed(1) and mouse_click_tick > mouse_click_delay:
                        game_state = 1
                        return game_state

                else:
                    sprite_fase1.draw()

                if sprite_fase2.x < pos_mouse[0] < (sprite_fase2.x + sprite_fase2.width):
                    sprite_fase2_hovered.draw()
                    if mouse.is_button_pressed(1) and mouse_click_tick > mouse_click_delay:
                        game_state = 2
                        return game_state

                else:
                    sprite_fase2.draw()


                # if sprite_sair.y < pos_mouse[1] < (sprite_sair.y + sprite_sair.height):
                #     sprite_sair_2.draw()
                #     if mouse.is_button_pressed(1) and mouse_click_tick > mouse_click_delay:
                #         exit()
                # else:
                #     sprite_sair.draw()

                mouse_click_tick = update_counters(mouse_click_tick)

            else:
                sprite_fase1.draw()
                sprite_fase2.draw()
                #sprite_sair.draw()
                update_counters(mouse_click_tick)

            window.update()
