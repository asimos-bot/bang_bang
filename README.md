# Bang Bang

Fast-paced turn-based game. At each turn you have the option to move
(up or down) and do an action (reload or shoot).

In single-player if the IA shoots you and you shoot it at the same time, you
are dead!

In multiplayer, the player to press the shoot button first wins!
