def shooting(player1, player2):

	if( player1.pos == player2.pos ):
	
		ready_player1 = player1.current_action == 'shoot' and player1.bullets>0
		ready_player2 = player2.current_action == 'shoot' and player2.bullets>0	
		
		if( ready_player1 ):
			player1.bullets-=1
			
			if( not ready_player2 ):
				player2.alive = False
				return False

		if( ready_player2 ):
			player2.bullets-=1
				
			if( not ready_player1 ):
				player1.alive = False
				return False
		return True
		
	else:
	
		if( player1.current_action == 'shoot' ) and player1.bullets > 0:
		
			player1.bullets-=1
			
		if( player2.current_action == 'shoot' )and player2.bullets > 0:
		
			player2.bullets-=1
		return False
		
			
